import sys
import time
import requests

# Change this if you want to change how often it updates the temps
sleep_interval = 1


#Dont Change anything below this lines.
PS3IP = sys.argv[1]
onlyTemp = False
print("Made by Lowikian")
if (len(sys.argv) < 2):
    print("Usage: 'ip'  'temp'")
    sys.exit(1)

if (len(sys.argv) < 3):
    print("Printing only temp!")
    onlyTemp = True

def isInt(s):
    Variable = s
    try:
        a = int(Variable)
        return True
    except:
        return False

if (onlyTemp == False):
    FanSpeed = sys.argv[2]
    if (FanSpeed != "auto"):
        if (isInt(FanSpeed) == False):
            print("FanSpeed is not an int or auto!")
            sys.exit(1)
        if (int(FanSpeed) > 100):
            FanSpeed = 100
else:
    FanSpeed = 100



PS3URL = f"http://{PS3IP}"
tempURLwithChange = f"{PS3URL}/cpursx.ps3?fan={FanSpeed}"
tempURLwithoutChange = f"{PS3URL}/cpursx_ps3"
print(f"Console URL: {PS3URL}")

# Maybe Change this 
def clear():
    print("\n"*100)
    



def getTemp(tempURL):
    print("Getting temp")
    while True:
        try:
            #clear()
            r = requests.get(tempURL)
            # Filter out the data
            cell_temp = r.text[476:-37]
            rsx_temp = r.text[488:-25]
        
            #Dosent work #fan_speed = 1

            # Uncomment one of the following lines to if you want to change the name of the cpu
            # print(f"Cell : {cell_temp} | RSX : {rsx_temp}")
            clear()
            print(f"CPU : {cell_temp} | GPU : {rsx_temp}")
        
            time.sleep(sleep_interval)
        except requests.ConnectionError as e:
            print("No response from server!")
            time.sleep(sleep_interval)
            #print(e)


def setTemp(tempURL):
    print("Setting temp")
    try:
        r = requests.get(tempURL)
        if (r.status_code != 200 ):
            print("Failed to set temp!")
        else:
            print("Success!")
    except requests.ConnectionError as e:
            print("No response from server!")


if(onlyTemp == False):
    setTemp(tempURLwithChange)
else:
    getTemp(tempURLwithoutChange)
